import pygame, sys
import time
import csv
from math import sin, cos, floor, fabs

class World:
    def __init__(self):
        self.wmap = []
        self.spritemap = []

        self.load("assets/map/wmap.csv",self.wmap,False)
        self.load("assets/map/spritemap.csv",self.spritemap,True)

    def load(self, file, destination,sprite):
        with open(file, newline='') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                if(sprite == False):        
                    t = []
                    for cell in row:
                        t.append(int(cell))
                    destination.append(t)
                else:
                    destination.append(Sprite(float(row[0]),float(row[1]),float(row[2])))


        # with open('spritemap.csv', newline='') as csvfile:
        #     self.spritemap = list(csv.reader(csvfile))

class Sprite:
    def __init__(self,x,y,texture):
        self.x = x
        self.y = y
        self.texture = texture
        self.distance = 0

class Camera:
    def __init__(self,width=160,height=120):
        # window width and height
        self.width = width
        self.height = height

        self.screen= pygame.display.set_mode((width*4, height*4),pygame.DOUBLEBUF)
        pygame.display.set_caption('Raycaster')
        self.buffer = pygame.Surface((width,height))
        # position in world
        self.xpos = float(22)
        self.ypos = float(12)
        # direction
        self.xdir = int(-1)
        self.ydir = int(1)
        # camera plane
        self.xplane = float(0)
        self.yplane = float(0.66)
        # frame times
        self.oldtime = float(0)
        self.time = float(0)

        # movement and rotation speed tied to framespeed
        self.movespeed = 0
        self.rotspeed = 0

        # texture image sizes
        self.texwidth = 64
        self.texheight = 64
        
        self.textures = [
            self.loadtexture(pygame.image.load('assets/textures/walls/bluestone.png').convert()),
            self.loadtexture(pygame.image.load('assets/textures/walls/colorstone.png').convert()),
            self.loadtexture(pygame.image.load('assets/textures/walls/eagle.png').convert()),
            self.loadtexture(pygame.image.load('assets/textures/walls/greystone.png').convert()),
            self.loadtexture(pygame.image.load('assets/textures/walls/mossy.png').convert()),
            self.loadtexture(pygame.image.load('assets/textures/walls/purplestone.png').convert()),
            self.loadtexture(pygame.image.load('assets/textures/walls/redbrick.png').convert()),
            self.loadtexture(pygame.image.load('assets/textures/walls/wood.png').convert()),
        ]

        self.spritetextures = [
            self.loadtexture(pygame.image.load('assets/textures/items/barrel.png').convert()),
            self.loadtexture(pygame.image.load('assets/textures/items/greenlight.png').convert()),
            self.loadtexture(pygame.image.load('assets/textures/items/pillar.png').convert()),
        ]

        self.floortextures = [
            self.loadfloor(pygame.image.load('assets/textures/walls/bluestone.png').convert()),
            self.loadfloor(pygame.image.load('assets/textures/walls/colorstone.png').convert()),
            self.loadfloor(pygame.image.load('assets/textures/walls/eagle.png').convert()),
            self.loadfloor(pygame.image.load('assets/textures/walls/greystone.png').convert()),
            self.loadfloor(pygame.image.load('assets/textures/walls/mossy.png').convert()),
            self.loadfloor(pygame.image.load('assets/textures/walls/purplestone.png').convert()),
            self.loadfloor(pygame.image.load('assets/textures/walls/redbrick.png').convert()),
            self.loadfloor(pygame.image.load('assets/textures/walls/wood.png').convert()),
        ]


        self.world = World()
        self.font = pygame.font.Font(pygame.font.get_default_font(), 12)
        self.zbuffer = []


    def loadfloor(self,image):
        ret = []
        for x in range(self.texwidth):
            row = []
            for y in range(self.texheight):
                s = pygame.Surface((1, 1)).convert()
                s.fill(image.get_at((x,y)))
                row.append(s)
            ret.append(row)
        return ret        
            


    def loadtexture(self,image):
        # Return tuple of texture slices, one regular and one dark
        ret = ([],[])
        for i in range(0,2,1):
            if i==1:
                image.set_alpha(128)
            for j in range(image.get_width()):
                s = pygame.Surface((1, image.get_height())).convert()
                s.blit(image, (- j, 0))
                ret[i].append(s)
        return ret


    def fps(self):
        self.oldtime = self.time
        self.time = pygame.time.get_ticks()

        frametime = float((self.time-self.oldtime)/1000.0)

        text = self.font.render(str(int(1.0/frametime)), True, (255, 255, 255))

        self.screen.blit(text, (0, 0))

        movespeed = frametime*5.0
        rotspeed = frametime*2.0     

        self.movespeed=movespeed
        self.rotspeed=rotspeed


    def forward(self):
        if(self.world.wmap[int(self.xpos+self.xdir*self.movespeed)][int(self.ypos)]==False):
            self.xpos +=self.xdir*self.movespeed
        if(self.world.wmap[int(self.xpos)][int(self.ypos+self.ydir*self.movespeed)]==False):
            self.ypos +=self.ydir*self.movespeed

    def left(self):
        # Rotate vector with:
        #[ cos(a) -sin(a) ]
        #[ sin(a)  cos(a) ]

        oldxdir = self.xdir
        self.xdir = self.xdir * cos(self.rotspeed)-self.ydir*sin(self.rotspeed)
        self.ydir=oldxdir*sin(self.rotspeed)+self.ydir*cos(self.rotspeed)
        oldxplane = self.xplane
        self.xplane = self.xplane *cos(self.rotspeed)-self.yplane*sin(self.rotspeed)
        self.yplane = oldxplane*sin(self.rotspeed)+self.yplane*cos(self.rotspeed)


    def back(self):
        if(self.world.wmap[int(self.xpos-self.xdir*self.movespeed)][int(self.ypos)]==False):
            self.xpos -= self.xdir * self.movespeed
        if(self.world.wmap[int(self.xpos)][int(self.ypos-self.ydir*self.movespeed)]==False):
            self.ypos -= self.ydir*self.movespeed
 
    def right(self):
        # Rotate vector with:
        #[ cos(a) -sin(a) ]
        #[ sin(a)  cos(a) ]

        oldxdir = self.xdir        
        self.xdir = self.xdir * cos(-self.rotspeed)-self.ydir*sin(-self.rotspeed)
        self.ydir = oldxdir *sin(-self.rotspeed)+self.ydir*cos(-self.rotspeed)
        oldxplane = self.xplane
        self.xplane = self.xplane*cos(-self.rotspeed)-self.yplane*sin(-self.rotspeed)
        self.yplane = oldxplane*sin(-self.rotspeed)+self.yplane*cos(-self.rotspeed)


    def raycast(self):
        self.buffer.fill((0,0,0))
        # pygame.draw.rect(self.screen,(0,112,112),(0,0,self.width,self.height/2))
        # pygame.draw.rect(self.screen,(112,112,112),(0,self.height/2,self.width,self.height/2))
        for x in range(0,self.width,1):
            xcamera = float(2.0*x/self.width-1)
            xraydir = float(self.xdir+(self.xplane*xcamera))
            yraydir = float(self.ydir+(self.yplane*xcamera))

            xmap = int(self.xpos)
            ymap = int(self.ypos)

            xdeltadist = float(abs(1/xraydir))
            ydeltadist = float(abs(1/yraydir))

            xstep=0
            ystep=0

            hit = 0
            side = 0

            if(xraydir <0):
                xstep = -1
                xsidedist = float((self.xpos-xmap)*xdeltadist)
            else:
                xstep = 1
                xsidedist = float((xmap+1.0-self.xpos)*xdeltadist)

            if(yraydir <0):
                ystep =-1
                ysidedist = float((self.ypos-ymap)*ydeltadist)
            else:
                ystep = 1
                ysidedist = float((ymap+1.0-self.ypos)*ydeltadist)

            while(hit==0):
                if(xsidedist<ysidedist):
                    xsidedist+=xdeltadist
                    xmap +=xstep
                    side = 0
                else:
                    ysidedist +=ydeltadist
                    ymap += ystep
                    side = 1

                if(self.world.wmap[xmap][ymap]>0):
                    hit =1

            if(side==0):
                perpwalldist=(xmap-self.xpos+(1-xstep)/2)/xraydir
            else:
                perpwalldist = (ymap-self.ypos+(1-ystep)/2)/yraydir

            # prevent 0 division error
            if perpwalldist == 0:
                perpwalldist = 0.000000000001
            lineheight = abs(int(self.height / perpwalldist))

            drawstart = - lineheight / 2 + self.height / 2
            drawend = lineheight / 2 + self.height / 2

            texnum = self.world.wmap[xmap][ymap]-1

            xwall=0
            if(side ==0):
                xwall = self.ypos+perpwalldist*yraydir
            else:
                xwall = self.xpos+perpwalldist * xraydir
            xwall-=floor(xwall)


            xtex = int(xwall*self.texwidth)
            if(side==0 and xraydir>0):
                xtex = self.texwidth -xtex -1
            if(side==1 and yraydir<0):
                xtex = self.texwidth - xtex -1


            if lineheight > 10000:
                lineheight=10000
                drawstart = -10000 /2 + self.height/2

            # pygame.draw.line(self.screen, color, (x,drawstart), (x,drawend), 1)


            xfloorwall = 0
            yfloorwall = 0
            if(side==0 and xraydir>0):
                xfloorwall = xmap
                yfloorwall = ymap+xwall
            elif(side==0 and xraydir<0):
                xfloorwall = xmap + 1.0
                yfloorwall = ymap + xwall
            elif(side == 1 and yraydir > 0):
                xfloorwall = xmap + xwall
                yfloorwall = ymap
            else:
                xfloorwall = xmap + xwall
                yfloorwall = ymap+1.0

            distwall = perpwalldist
            distplayer = 0.0

            if(drawend<0):
                drawend = int(self.height)

            for y in range(int(drawend)+1,self.height,1):
                currentdist = self.height / (2.0*y-self.height)
                weight = (currentdist-distplayer)/(distwall-distplayer)
                
                xfloorcurrent = weight*xfloorwall+(1.0-weight)*self.xpos
                yfloorcurrent = weight*yfloorwall+(1.0-weight)*self.ypos

                xfloortex = int(xfloorcurrent*self.texwidth)%self.texwidth
                yfloortex = int(yfloorcurrent*self.texheight)%self.texheight

                self.buffer.blit(self.floortextures[3][xfloortex][yfloortex],(x,y))
                self.buffer.blit(self.floortextures[3][xfloortex][yfloortex],(x,self.height-y))

           
            self.buffer.blit(pygame.transform.scale(self.textures[texnum][side][xtex], (1, lineheight)), (x, drawstart))


            for i in range(0,len(self.world.spritemap),1):
                sprite = self.world.spritemap[i]
                sprite.distance = ((self.xpos - sprite.x)*(self.xpos-sprite.x)+(self.ypos-sprite.y)*(self.ypos-sprite.y))

            self.world.spritemap.sort(key=lambda x: x.distance)


#     //after sorting the sprites, do the projection and draw them
#     for(int i = 0; i < numSprites; i++)
#     {
#       //translate sprite position to relative to camera
#       double spriteX = sprite[spriteOrder[i]].x - posX;
#       double spriteY = sprite[spriteOrder[i]].y - posY;



            pygame.transform.scale(self.buffer, (self.width*4, self.height*4), self.screen)
            # self.screen.blit(self.buffer,(0,0))


            # self.screen.blit(pygame.transform.scale(self.buffer,(self.width*2,self.height*2)),(0,0))




if __name__ == '__main__':
    pygame.init()
    
    camera = Camera()

    # Event loop
    while True:
        camera.raycast()
        camera.fps()

        pygame.display.flip()

        keys = pygame.key.get_pressed() 
        if keys[pygame.K_w]: 
            camera.forward()
        if keys[pygame.K_a]: 
            camera.left()
        if keys[pygame.K_s]: 
            camera.back()
        if keys[pygame.K_d]: 
            camera.right()                        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

