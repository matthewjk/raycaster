from pyglet import *
from pyglet.gl import *
from pyglet.window import key, mouse

from typing import Tuple 
from math import sin, cos, floor, fabs
import csv, time

width = 640
height = 480

class Line():
    def __init__(self, start:Tuple[int,int],end:Tuple[int,int],width:int = 1,color:Tuple[int,int,int,int] = (255,255,255,255)):
        # Coordinates from top left
        self.x1 = start[0]
        self.y1 = height-start[1]
        self.x2 = end[0]
        self.y2 = height-end[1]
        self.width = width
        self.color = color 

    def __repr__(self):
        return "Line: ("+str(self.x1)+","+str(self.y1)+") to ("+str(self.x2)+","+str(self.y2)+")"

    def draw(self):
        pyglet.graphics.draw(2,
         GL_LINES,
        ('v2f',(self.x1,self.y1,self.x1,self.y2)),
        ('c4B', (self.color[0], self.color[1], self.color[2], self.color[3]) * 2))


class Label():
    def __init__(self,text:str,pos:Tuple[int,int],size:int,color:Tuple[int,int,int,int]=(255,255,255,255),mode:Tuple[str,str] = ("left","top"),font:str = "arial"):
        self.text = text
        # Coordinates from top left
        self.x = pos[0]
        self.y = height - pos[1]
        self.size = size
        self.xmode = mode[0]
        self.ymode = mode[1]
        self.font = font
        self.color = color
        self.label = pyglet.text.Label(self.text,
                            font_name=self.font,
                            font_size=self.size,
                            x=self.x, y=self.y,
                            color = self.color,
                            anchor_x=self.xmode, anchor_y=self.ymode)
    
    def __repr__(self):
        return "Text: '"+str(self.text)+"' at ("+str(self.x)+","+str(self.y)+")"
    
    def draw(self):
        self.label.draw()

    def updatetext(self,text:str):
        self.text=text
        self.label.text = self.text


class Texture():
    def __init__(self, file:str,size:int = 64):
        self.file = file
        self.size = 64
        self.texture = image.load(file)
        self.slices = []
        self.pixelarray = []
        self.load()

    def __repr__(self):
        return "Texture: "+self.file
        
    def load(self):
        for x in range(0,self.size,1):
            self.slices.append(self.texture.get_region(x,0,1,self.size).get_texture())
            self.slices[x].anchor_y = self.size

        for x in range(0,self.size,1):
            row = []
            for y in range(0,self.size,1):
                row.append(self.texture.get_region(x,y,1,1))
            self.pixelarray.append(row)



    def draw(self,s:int,start:Tuple[int,int],end:Tuple[int,int],xscale:int = 1):
        # Coordinates from top left
        x1 = start[0]
        y1 = height - start[1]
        x2 = end[0]
        y2 = height - end[1] 

        ### OPTION 3
        # texture = self.slices[s]
        # texture.anchor_y= 0
        # texture.height = y2 - y1
        # texture.blit(x1,y1)


        ### OPTION 2
        # texture = self.slices[s]
        # glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST)
        # glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST)
        # texture = pyglet.graphics.TextureGroup(texture)
        # batch = pyglet.graphics.Batch()
        # tex_coords = ('t2f',(0,0, 1,0, 1,1, 0,1, ))
        # vertices = ('v3f',(x2,y2,0,x2+xscale,y2,0,x1+xscale,y1,0,x1,y1,0))
        # batch.add(4,GL_QUADS,texture,vertices,tex_coords)
        # batch.draw()

        ### OPTION 1
        texture = self.slices[s]
        glBindTexture(GL_TEXTURE_2D, texture.id)
        glEnable(GL_TEXTURE_2D)
        glBegin(GL_QUADS);
        glTexCoord2f(0,0)
        glVertex3f(x2,y2,0);
        glTexCoord2f(1,0)
        glVertex3f(x2+xscale,y2,0);
        glTexCoord2f(1,1)
        glVertex3f(x1+xscale,y1,0);
        glTexCoord2f(0,1)
        glVertex3f(x1,y1,0);
        glEnd();
        glDisable(GL_TEXTURE_2D)

class Sprite:
    def __init__(self,x,y,texture):
        self.x = x
        self.y = y
        self.texture = texture
        self.distance = 0

    def __repr__(self):
        return "Sprite: "+str(self.texture)+" at ("+str(self.x)+","+str(self.y)+")"


class World:
    def __init__(self):
        self.worldmap = []
        self.spritemap = []
        self.textures = []

        self.loadworld("assets/map/wmap.csv",self.worldmap)
        self.loadsprites("assets/map/spritemap.csv",self.spritemap)
        self.loadtextures(self.textures)

    def loadworld(self, file:str, destination:list):
        with open(file, newline='') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                destination.append(list(map(int,row)))

    def loadsprites(self,file:str,destination:list):
        with open(file, newline='') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                destination.append(Sprite(float(row[0]),float(row[1]),int(row[2])))

    def loadtextures(self,destination:list):
        print("Loading textures")
        destination.append(Texture('assets/textures/walls/bluestone.png',64))
        destination.append(Texture('assets/textures/walls/colorstone.png',64))
        destination.append(Texture('assets/textures/walls/eagle.png',64))
        destination.append(Texture('assets/textures/walls/greystone.png',64))
        destination.append(Texture('assets/textures/walls/mossy.png',64))
        destination.append(Texture('assets/textures/walls/purplestone.png',64))
        destination.append(Texture('assets/textures/walls/redbrick.png',64))
        destination.append(Texture('assets/textures/walls/wood.png',64))
        print("Finished loading")


class Camera():
    def __init__(self,width:int=width,height:int=height):
        self.width=width
        self.height=height
        
        # position in world
        self.xpos = float(22)
        self.ypos = float(12)
        # direction
        self.xdir = int(-1)
        self.ydir = int(1)
        # camera plane
        self.xplane = float(0)
        self.yplane = float(0.66)

        # movement and rotation speed tied to framespeed
        self.movespeed = 0
        self.rotspeed = 0

        self.frametime = clock.get_fps()
        self.fpslabel = Label(str(int(self.frametime)),(10,10),12)
        self.fpslabel.draw()

        self.World = World()

        self.texwidth = 64
        self.texheight = 64

        self.wallbuffer = []

    def update(self,dt,keys):
        if keys[key.W]:
            self.forward()
        if keys[key.A]:
            self.left()
        if keys[key.S]:
            self.back()
        if keys[key.D]:
            self.right()

    def fps(self):
        self.frametime = clock.get_fps()
        self.fpslabel.updatetext(str(int(self.frametime)))
        self.fpslabel.draw()

        if(self.frametime==0):
            self.frametime+=0.0001

        self.movespeed=(1/self.frametime)*5.0
        self.rotspeed=(1/self.frametime)*2.0

    def forward(self):
        if(self.World.worldmap[int(self.xpos+self.xdir*self.movespeed)][int(self.ypos)]==False):
            self.xpos +=self.xdir*self.movespeed
        if(self.World.worldmap[int(self.xpos)][int(self.ypos+self.ydir*self.movespeed)]==False):
            self.ypos +=self.ydir*self.movespeed

    def left(self):
        # Rotate vector with:
        #[ cos(a) -sin(a) ]
        #[ sin(a)  cos(a) ]
        oldxdir = self.xdir
        self.xdir = self.xdir * cos(self.rotspeed)-self.ydir*sin(self.rotspeed)
        self.ydir=oldxdir*sin(self.rotspeed)+self.ydir*cos(self.rotspeed)
        oldxplane = self.xplane
        self.xplane = self.xplane *cos(self.rotspeed)-self.yplane*sin(self.rotspeed)
        self.yplane = oldxplane*sin(self.rotspeed)+self.yplane*cos(self.rotspeed)

    def back(self):
        if(self.World.worldmap[int(self.xpos-self.xdir*self.movespeed)][int(self.ypos)]==False):
            self.xpos -= self.xdir * self.movespeed
        if(self.World.worldmap[int(self.xpos)][int(self.ypos-self.ydir*self.movespeed)]==False):
            self.ypos -= self.ydir*self.movespeed
 
    def right(self):
        # Rotate vector with:
        #[ cos(a) -sin(a) ]
        #[ sin(a)  cos(a) ]
        oldxdir = self.xdir        
        self.xdir = self.xdir * cos(-self.rotspeed)-self.ydir*sin(-self.rotspeed)
        self.ydir = oldxdir *sin(-self.rotspeed)+self.ydir*cos(-self.rotspeed)
        oldxplane = self.xplane
        self.xplane = self.xplane*cos(-self.rotspeed)-self.yplane*sin(-self.rotspeed)
        self.yplane = oldxplane*sin(-self.rotspeed)+self.yplane*cos(-self.rotspeed)

    def raycast(self):
        # texbuffer = pyglet.image.Texture.create(width=width,height=height,rectangle=True)
        # pygame.draw.rect(self.screen,(0,112,112),(0,0,self.width,self.height/2))
        # pygame.draw.rect(self.screen,(112,112,112),(0,self.height/2,self.width,self.height/2))
        for x in range(0,self.width,1):
            xcamera = float(2.0*x/self.width-1)
            xraydir = float(self.xdir+(self.xplane*xcamera))
            yraydir = float(self.ydir+(self.yplane*xcamera))

            xmap = int(self.xpos)
            ymap = int(self.ypos)

            xdeltadist = float(abs(1/xraydir))
            ydeltadist = float(abs(1/yraydir))

            xstep=0
            ystep=0

            hit = 0
            side = 0
            if(xraydir <0):
                xstep = -1
                xsidedist = float((self.xpos-xmap)*xdeltadist)
            else:
                xstep = 1
                xsidedist = float((xmap+1.0-self.xpos)*xdeltadist)

            if(yraydir <0):
                ystep =-1
                ysidedist = float((self.ypos-ymap)*ydeltadist)
            else:
                ystep = 1
                ysidedist = float((ymap+1.0-self.ypos)*ydeltadist)

            while(hit==0):
                if(xsidedist<ysidedist):
                    xsidedist+=xdeltadist
                    xmap +=xstep
                    side = 0
                else:
                    ysidedist +=ydeltadist
                    ymap += ystep
                    side = 1

                if(self.World.worldmap[xmap][ymap]>0):
                    hit =1

            if(side==0):
                perpwalldist=(xmap-self.xpos+(1-xstep)/2)/xraydir
            else:
                perpwalldist = (ymap-self.ypos+(1-ystep)/2)/yraydir

            # prevent 0 division error
            if perpwalldist == 0:
                perpwalldist = 0.000000000001
            lineheight = abs(int(self.height / perpwalldist))

            drawstart = - lineheight / 2 + self.height / 2
            drawend = lineheight / 2 + self.height / 2

            texnum = self.World.worldmap[xmap][ymap]-1

            xwall=0
            if(side ==0):
                xwall = self.ypos+perpwalldist*yraydir
            else:
                xwall = self.xpos+perpwalldist * xraydir
            xwall-=floor(xwall)

            xtex = int(xwall*self.texwidth)
            if(side==0 and xraydir>0):
                xtex = self.texwidth -xtex -1
            if(side==1 and yraydir<0):
                xtex = self.texwidth - xtex -1


            if lineheight > 10000:
                lineheight=10000
                drawstart = -10000 /2 + self.height/2


            xfloorwall = 0
            yfloorwall = 0
            if(side==0 and xraydir>0):
                xfloorwall = xmap
                yfloorwall = ymap+xwall
            elif(side==0 and xraydir<0):
                xfloorwall = xmap + 1.0
                yfloorwall = ymap + xwall
            elif(side == 1 and yraydir > 0):
                xfloorwall = xmap + xwall
                yfloorwall = ymap
            else:
                xfloorwall = xmap + xwall
                yfloorwall = ymap+1.0

            distwall = perpwalldist
            distplayer = 0.0

            if(drawend<0):
                drawend = int(self.height)

            self.World.textures[texnum].draw(xtex,(x,drawstart),(x,drawend))
            
            for y in range(int(drawend)+1,self.height,1):
                currentdist = self.height / (2.0*y-self.height)
                weight = (currentdist-distplayer)/(distwall-distplayer)
                
                xfloorcurrent = weight*xfloorwall+(1.0-weight)*self.xpos
                yfloorcurrent = weight*yfloorwall+(1.0-weight)*self.ypos

                xfloortex = int(xfloorcurrent*self.texwidth)%self.texwidth
                yfloortex = int(yfloorcurrent*self.texheight)%self.texheight

                self.World.textures[3].pixelarray[xfloortex][yfloortex].blit(x,height-y)                                
                self.World.textures[3].pixelarray[xfloortex][yfloortex].blit(x,y)
               
                # texbuffer.blit_into(self.World.textures[3].pixelarray[xfloortex][yfloortex],x,y,0)
                # texbuffer.blit_into(self.World.textures[3].pixelarray[xfloortex][yfloortex],x,height-y,0)

                # self.World.textures[3].pixelarray[xfloortex][yfloortex].blit_into(texbuffer,x,y,0)
            
            
        # texbuffer.blit(0,0,0)


 

            # for i in range(0,len(self.world.spritemap),1):
            #     sprite = self.world.spritemap[i]
            #     sprite.distance = ((self.xpos - sprite.x)*(self.xpos-sprite.x)+(self.ypos-sprite.y)*(self.ypos-sprite.y))

            # self.world.spritemap.sort(key=lambda x: x.distance)

class Window(pyglet.window.Window):
    def setLock(self,state): self.lock = state; self.set_exclusive_mouse(state)
    lock = False
    mouse_lock = property(lambda self:self.lock,setLock)

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.set_minimum_size(640,480)
        self.keys = key.KeyStateHandler()
        self.push_handlers(self.keys)
        pyglet.clock.schedule(self.update)

        self.Camera = Camera(width,height)

    def on_mouse_motion(self,x,y,dx,dy):
        print(str(x)+","+str(y))
        # if self.mouse_lock:
        #     self.player.mouse_motion(dx,dy)

    def on_key_press(self,KEY,MOD):
        if KEY == key.ESCAPE:
            self.close()
        # elif KEY == key.E:
        #     self.mouse_lock = not self.mouse_lock

    def update(self,dt):
        self.Camera.update(dt,self.keys)
        # self.player.update(dt,self.keys)

    def on_draw(self):
        self.clear()
        self.Camera.raycast()
        self.Camera.fps()


if __name__ == '__main__':
    window = Window(width=width,height=height,caption='Raycaster',resizable=False)
    glClearColor(0,0,0,255)
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_NEAREST)
    pyglet.app.run()
