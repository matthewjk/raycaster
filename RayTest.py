import pygame, sys
import math

class Map:
    def __init__(self):
        self.plan = [
            [1,1,1,1,1,1,1,1],
            [1,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,1],
            [1,0,0,0,0,0,0,1],
            [1,1,1,1,1,1,1,1]
        ]


class Camera:
    def __init__(self):
        self.map = Map()

        # Position in world from top left
        self.x = 96
        self.y = 224
        # View angle
        #        90
        #        |
        # 180--------0
        #       |
        #      270
        self.a = 270
        self.fov = 60

        self.screenwidth = 640
        self.screenheight = 480

        self.screen= pygame.display.set_mode((self.screenwidth, self.screenheight))
        pygame.display.set_caption('Raycaster')

        self.castrays()

    # def draw(self):
    #     self.screen.fill((0,0,0))
    #     cellsize = 64
    #     for x in range(0,len(self.map.plan),1):
    #         for y in range(0,len(self.map.plan[0]),1):
    #             if(self.map.plan[x][y]!=0):
    #                 pygame.draw.rect(self.screen,(0,0,255),(x*cellsize,y*cellsize,cellsize,cellsize),0)
    #             pygame.draw.rect(self.screen,(255,255,255),(x*cellsize,y*cellsize,cellsize,cellsize),1)

        # lstart = (self.x,self.y)
        # # Horizontal grid intersections



        # pygame.draw.line(self.screen, (255,0,0), lstart, (vertx,verty), 2)
        # pygame.draw.line(self.screen, (255,0,0), lstart, (horizx,horizy), 2)




    def castrays(self):
        cellsize = 64
        for x in range(0,len(self.map.plan),1):
            for y in range(0,len(self.map.plan[0]),1):
                if(self.map.plan[x][y]!=0):
                    pygame.draw.rect(self.screen,(0,0,255),(x*cellsize,y*cellsize,cellsize,cellsize),0)
                pygame.draw.rect(self.screen,(255,255,255),(x*cellsize,y*cellsize,cellsize,cellsize),1)


        alpha = self.a + self.fov/2
        beta = self.fov/2
        increment = -(self.fov/self.screenwidth)
        for x in range(0,self.screenwidth,1):
            raydist = Ray(self.x,self.y,alpha,beta,self.screen).cast()
            self.drawwall(x,raydist)
            alpha += increment
            beta += increment
            if(alpha>360):
                alpha-=360
            elif(alpha<0):
                alpha+=360

    def degtorad(self,deg):
        return deg * math.pi / 180.0  

    def drawwall(self,x, raydist):
        # wall height = 64
        projectionpane = (self.screenwidth/2)/math.tan(self.degtorad(self.fov/2))
        lineheight = 64/raydist*projectionpane

        drawstart = - lineheight / 2 + self.screenheight / 2
        drawend = lineheight / 2 + self.screenheight / 2


        pygame.draw.line(self.screen,(255,255,255),(x,drawstart),(x,drawend),1)


    def left(self):
        if(self.a==0):
            self.a=359
        else:
            self.a-=1
    
    def right(self):
        if(self.a==359):
            self.a=0
        else:
            self.a+=1



class Ray:
    def __init__(self,x,y,alpha,beta,screen):
        # Origin (x,y)
        self.x = x
        self.y = y
        # View angle from origin
        self.alpha = alpha+0.000001
        self.beta = beta
        

        ## Change how this is being done
        self.fov = 60
        self.map = Map()
        self.screen = screen
    
    def degtorad(self,deg):
        return deg * math.pi / 180.0    



    def cast(self):
        cellsize = 64
        # self.screen.fill((0,0,0))

        # Horizontal grid intersection
        if(self.alpha>=0 and self.alpha<=180):
            horizy = math.floor(self.y/cellsize)*(cellsize)-1
            horizya = -cellsize
        else:
            horizy = math.floor(self.y/cellsize)*(cellsize)+cellsize
            horizya = cellsize
        horizx = self.x + (self.y-horizy)/math.tan(self.degtorad(self.alpha));
        horizxa = cellsize/math.tan(self.degtorad(self.alpha))


        # Vertical grid intersections
        if(self.alpha <=90 or self.alpha>=270):
            vertx = math.floor(self.x/cellsize)*cellsize+cellsize
            vertxa = +cellsize
        else:
            vertx = math.floor(self.x/cellsize)*(cellsize)-1
            vertxa = -cellsize
        verty = self.y + (self.x-vertx)*math.tan(self.degtorad(self.alpha))
        vertya = cellsize*math.tan(self.degtorad(self.alpha))

        # Compute horizontal distance
        horizdist = -1
        x = math.floor(horizx/cellsize)
        y = math.floor(horizy/cellsize)
        while(x >= 0 and x<8 and y>=0 and y<8):
            if(self.map.plan[x][y]==1):
                # horizdist = abs(self.x-horizx/math.cos(self.degtorad(self.alpha)))
                  # TEST THIS: perpwalldist=(xmap-self.xpos+(1-xstep)/2)/xraydir
                horizdist = math.sqrt(math.pow((self.x-horizx),2)+math.pow((self.y-horizy),2))
                horizdist = horizdist*math.cos(self.degtorad(self.beta))
                break
            else:
                horizx = math.floor(horizx+horizxa)
                horizy = math.floor(horizy+horizya)
            x = math.floor(horizx/cellsize)
            y = math.floor(horizy/cellsize)


        # Compute vertical distance
        vertdist = -1
        x = math.floor(vertx/cellsize)
        y = math.floor(verty/cellsize)
        while(x >= 0 and x<8 and y>=0 and y<8):
            if(self.map.plan[x][y]==1):
                # vertdist = abs(self.x-vertx/math.cos(self.degtorad(self.alpha)))
                vertdist = math.sqrt(math.pow((self.x-vertx),2)+math.pow((self.y-verty),2))
                vertdist = vertdist*math.cos(self.degtorad(self.beta))
                # print("Coords: "+str(math.floor(vertx/64))+","+str(math.floor(verty/64)))
                break
            else:
                vertx = math.floor(vertx+vertxa)
                verty = math.floor(verty+vertya)
            x = math.floor(vertx/cellsize)
            y = math.floor(verty/cellsize)

        
        # pygame.draw.line(self.screen, (255,0,0), (self.x,self.y), (vertx,verty), 1)
        # pygame.draw.line(self.screen, (0,255,0), (self.x,self.y), (horizx,horizy), 1)


        if(horizdist<=vertdist or vertdist ==-1):
            pygame.draw.line(self.screen, (255,0,0), (self.x,self.y), (horizx,horizy), 1)
            return horizdist
        elif(vertdist<=horizdist or horizdist==-1):
            pygame.draw.line(self.screen, (0,255,0), (self.x,self.y), (vertx,verty), 1)
            return vertdist
        else:
            print("Alpha: "+str(self.alpha))
            print(str(horizdist)+ ","+str(vertdist))
            



if __name__ == '__main__':
    pygame.init()
    camera = Camera()

    # Event loop
    while True:
        # Do drawing here
        # camera.draw()

        pygame.display.flip()

        keys = pygame.key.get_pressed() 
        # if keys[pygame.K_w]: 
        #     camera.forward()
        if keys[pygame.K_a]: 
            camera.left()
        # if keys[pygame.K_s]: 
        #     camera.back()
        if keys[pygame.K_d]: 
            camera.right()                        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

