// g++ SDLCaster.cpp -o SDLCaster -lSDL2 -lSDL2_image -lSDL2_ttf
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
// #include <SDL2/SDL_ttf.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
// for profiling functions
#include <chrono>

#define SCREEN_WIDTH 480
#define SCREEN_HEIGHT 360

#define TEXTURE_WIDTH 64
#define TEXTURE_HEIGHT 64

Uint32 createpixel(Uint8 red, Uint8 green, Uint8 blue){
    Uint32 color = 0;
    color += red;
    color <<= 8;
    color += green;
    color <<= 8;
    color += blue;
    color <<= 8;
    color += 0xFF;
    return color;
}

Uint32 getpixel( SDL_Surface *surface, int x, int y )
{
    Uint8 *pixel = (Uint8 *)surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel;
    return *(Uint32 *)pixel;
}

void setpixel( SDL_Surface *surface, int x, int y, Uint32 pixel )
{
    Uint8 *pixels = (Uint8 *)surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel;
    *(Uint32 *)pixels = pixel;
}


class Texture{
    private:
        std::string _file = "";
        int _width = TEXTURE_WIDTH;
        int _height = TEXTURE_HEIGHT;
        SDL_Surface* _image;
        Uint32 _pixels[TEXTURE_WIDTH][TEXTURE_WIDTH];  

    public:
        Texture(std::string f){
            _file = f;
            loadtexture();
        }
        ~Texture(){};

        void loadtexture(){
            std::cout<<"Loading texture: "<<_file<<std::endl;
            // file locations needs to be char* not string
            _image = IMG_Load(_file.c_str());
            if(_image==NULL){
                std::cout<<"Failed to load texture: "<<_file<<std::endl;
            }
            for(int x=0;x<_width;x++){
                for(int y=0;y<_height;y++){
                    _pixels[x][y]=getpixel(_image,x,y);
                }
            }
        }

        Uint32 gettexturepixel(int x, int y){
            return _pixels[x][y];
        }
};

class World{
    private:
        // int worldmap[24][24];
        std::vector<std::vector<int> > _worldmap;
        std::vector<Texture> _textures;

        void loadworld(){
            std::cout<<"Loading world"<<std::endl;
            std::ifstream file("assets/map/wmap.csv");
            std::string line;
            while(std::getline(file,line))
            {
                std::stringstream lineStream(line);
                std::string cell;
                std::vector<int> row;
                while(std::getline(lineStream,cell,','))
                {
                    int tempint = 0;
                    std::stringstream tempss(cell);
                    tempss >> tempint; 
                    row.push_back(tempint);
                }
                _worldmap.push_back(row);
            }
            // std::cout<<"Finished loading world"<<std::endl;
        }

        void loadtextures(){
            _textures.push_back(Texture("./assets/textures/walls/bluestone.png"));
            _textures.push_back(Texture("./assets/textures/walls/colorstone.png"));
            _textures.push_back(Texture("./assets/textures/walls/eagle.png"));
            _textures.push_back(Texture("./assets/textures/walls/greystone.png"));
            _textures.push_back(Texture("./assets/textures/walls/mossy.png"));
            _textures.push_back(Texture("./assets/textures/walls/purplestone.png"));
            _textures.push_back(Texture("./assets/textures/walls/redbrick.png"));
            _textures.push_back(Texture("./assets/textures/walls/wood.png"));
        }

    public:
        World(){
            loadworld();
            loadtextures();
        }
        ~World(){};

        void printworld(){
            for(int x=0;x<_worldmap.size();x++){
                for(int y=0; y<_worldmap[x].size();y++){
                    std::cout<<_worldmap[x][y];
                }
                std::cout<<std::endl;
            }
        }

        int getwall(int x, int y){
            return _worldmap[x][y];
        }

        Texture gettexture(int index){
            return _textures[index];
        }

};

class Camera{
    private:

        SDL_Surface* initpixelbuffer(){
            SDL_Surface* format = NULL;
            SDL_Surface* surface = NULL;
            format = IMG_Load("./assets/textures/walls/eagle.png");
            if(format==NULL){
                std::cout<<"Format load unsuccessful"<<std::endl;
            }
            if(format->flags){
                surface = SDL_CreateRGBSurface( SDL_SWSURFACE, SCREEN_WIDTH, SCREEN_HEIGHT, format->format->BitsPerPixel, format->format->Rmask, format->format->Gmask, format->format->Bmask, 0 );
            }
            else{
                surface = SDL_CreateRGBSurface( SDL_SWSURFACE, SCREEN_WIDTH, SCREEN_HEIGHT, format->format->BitsPerPixel, format->format->Rmask, format->format->Gmask, format->format->Bmask, format->format->Amask );

            }
            return surface;
        }

        SDL_Window* _window = NULL;
        SDL_Surface* _windowsurface = NULL;
        SDL_Surface* _pixelbuffer = initpixelbuffer();

        // TTF_Font* font = TTF_OpenFont("roboto.ttf", 24);
        // SDL_Color white = {255,255,255,0};
        // std::string text = "testing";
        // SDL_Surface* testmessage = TTF_RenderText_Solid(font, text.c_str(), white);


        int _width = SCREEN_WIDTH;
        int _height = SCREEN_HEIGHT;

        int _texwidth = TEXTURE_WIDTH;
        int _texheight = TEXTURE_HEIGHT;

        World _world;
        
        // position in world
        double _xpos = 22.0;
        double _ypos = 11.5;

        // direction
        double _xdir = -1.0;
        double _ydir = 0.0;

        // camera plane
        double _xplane = 0.0; 
        double _yplane = 0.66;

        // movement and rotation speed tied to framespeed
        double _movespeed = 0;
        double _rotspeed = 0;

        double _time;
        double _oldtime;

    public:
        Camera(SDL_Window* w){
            _window = w;
            _windowsurface = SDL_GetWindowSurface(_window);
        }
        ~Camera(){}

    void raycast(){
        if( SDL_MUSTLOCK(_pixelbuffer)){
            SDL_LockSurface(_pixelbuffer );
        }
        for(int x = 0; x < _width; x++){
            // get ray position and direction
            double cameraX = 2 * x / double(_width) - 1; //x-coordinate in camera space
            double xraydir = _xdir + _xplane * cameraX;
            double yraydir = _ydir + _yplane * cameraX;

            // get cell player is on
            int xmap = int(_xpos);
            int ymap = int(_ypos);

            // distance of ray to next x/y side
            double xsidedist;
            double ysidedist;

            // distance between x and y sides
            double xdeltadist = std::abs(1 / xraydir);
            double ydeltadist = std::abs(1 / yraydir);
            double perpwalldist;

            // direction to step in +1 or -1
            int xstep;
            int ystep;

            // whether wall was hit and which side
            int hit = 0;
            int side;

            // calculate step and sidedist
            if (xraydir < 0){
                xstep = -1;
                xsidedist = (_xpos - xmap) * xdeltadist;
            }
            else{
                xstep = 1;
                xsidedist = (xmap + 1.0 - _xpos) * xdeltadist;
            }
            if (yraydir < 0){
            ystep = -1;
            ysidedist = (_ypos - ymap) * ydeltadist;
            }
            else{
                ystep = 1;
                ysidedist = (ymap + 1.0 - _ypos) * ydeltadist;
            }

            // find nearest wall
            while (hit == 0){
                // move to next square boundary
                if (xsidedist < ysidedist){
                    xsidedist += xdeltadist;
                    xmap += xstep;
                    side = 0;
                }
                else{
                    ysidedist += ydeltadist;
                    ymap += ystep;
                    side = 1;
                }
                // check if a wall was hit
                if (_world.getwall(xmap,ymap) > 0){
                    hit = 1;
                }
            }

            // perpendicular ray distance
            if (side == 0){
                perpwalldist = (xmap - _xpos + (1 - xstep) / 2) / xraydir;
            }
            else{
                perpwalldist = (ymap - _ypos + (1 - ystep) / 2) / yraydir;
            }

            // height of wall to draw
            int lineheight = (int)(_height / perpwalldist);

            // position of highest and lowest pixel
            int drawStart = -lineheight / 2 + _height / 2;
            if(drawStart < 0){
                drawStart = 0;
            }
            int drawend = lineheight / 2 + _height / 2;
            if(drawend >= _height){
                drawend = _height - 1;
            }
            // which texture to draw (-1 to make use of 0)
            int texnum = _world.getwall(xmap,ymap)-1;

            // position where wall was hit
            double xwall;
            if (side == 0){
                xwall = _ypos + perpwalldist * yraydir;
            }
            else{
                xwall = _xpos + perpwalldist * xraydir;
            }
            xwall -= floor((xwall));

            // texture x coordinates
            int xtex = int(xwall * double(_texwidth));
            if(side == 0 && xraydir > 0){
                xtex = _texwidth - xtex - 1;
            }
            if(side == 1 && yraydir < 0){
                xtex = _texwidth - xtex - 1;
            }

            for(int y = drawStart; y < drawend; y++){
                // 256 and 128 factors to avoid floats
                int d = y * 256 - _height * 128 + lineheight * 128; 
                int ytex = ((d * _texheight) / lineheight) / 256;

                Uint32 pixel = _world.gettexture(texnum).gettexturepixel(xtex,ytex);
                // make y side darker
                if(side==1){
                    pixel = (pixel>>1) & 8355711;
                }
                setpixel(_pixelbuffer, x, y, pixel );
            }

            // position at bottom of wall
            double xfloorwall, yfloorwall;

            // work out which wall direction
            if(side == 0 && xraydir > 0){
                xfloorwall = xmap;
                yfloorwall = ymap + xwall;
            }
            else if(side == 0 && xraydir < 0){
                xfloorwall = xmap + 1.0;
                yfloorwall = ymap + xwall;
            }
            else if(side == 1 && yraydir > 0){
                xfloorwall = xmap + xwall;
                yfloorwall = ymap;
            }
            else{
                xfloorwall = xmap + xwall;
                yfloorwall = ymap + 1.0;
            }

            double walldist, playerdist, currentdist;

            walldist = perpwalldist;
            playerdist = 0.0;

            // prevent overflow
            if (drawend < 0){
                drawend = _height;
            }

            // draw floor from bottom of wall
            for(int y = drawend + 1; y < _height; y++){
                currentdist = _height / (2.0 * y - _height);

                double weight = (currentdist - playerdist) / (walldist - playerdist);

                double xfloorcurrent = weight * xfloorwall + (1.0 - weight) * _xpos;
                double yfloorcurrent = weight * yfloorwall + (1.0 - weight) * _ypos;

                int xfloortex, yfloortex;
                xfloortex = int(xfloorcurrent * _texwidth) % _texwidth;
                yfloortex = int(yfloorcurrent * _texheight) % _texheight;

                // stripe pattern
                int pattern = (int(xfloorcurrent + yfloorcurrent)) % 2;
                // checkerboard pattern
                // int pattern = (int(xfloorcurrent) + int(yfloorcurrent)) % 2;
                int floortexnum;
                if(pattern == 0){
                    floortexnum = 3;
                }
                else{
                    floortexnum = 4;
                }

                Uint32 pixel = _world.gettexture(floortexnum).gettexturepixel(xfloortex,yfloortex);

                setpixel(_pixelbuffer, x, y, pixel );
                setpixel(_pixelbuffer, x, _height-y, pixel );

            }
        }
        
        SDL_BlitSurface(_pixelbuffer,NULL,_windowsurface,NULL);
        // SDL_BlitSurface(_testmessage,NULL,_windowsurface,NULL);

        if(SDL_MUSTLOCK(_pixelbuffer)){
            SDL_UnlockSurface( _pixelbuffer );
        }
        SDL_UpdateWindowSurface(_window);
    }

    void fps(){
        _oldtime = _time;
        _time = SDL_GetTicks();
        double frametime = (_time - _oldtime) / 1000.0; //frametime is the time this frame has taken, in seconds
        // print(1.0 / frametime); //FPS counter


        _movespeed = frametime * 3.0; //the constant value is in squares/second
        _rotspeed = frametime * 2.0; //the constant value is in radians/second    
    }

    void forward(){
        if(_world.getwall(int(_xpos + _xdir * _movespeed),int(_ypos))==false){
            _xpos += _xdir * _movespeed;
        }
        if(_world.getwall(int(_xpos),int(_ypos + _ydir * _movespeed))==false){
            _ypos += _ydir * _movespeed;
        }
    }

    void left(){
        //both camera direction and camera plane must be rotated
        double oldxdir = _xdir;
        _xdir = _xdir * cos(_rotspeed) - _ydir * sin(_rotspeed);
        _ydir = oldxdir * sin(_rotspeed) + _ydir * cos(_rotspeed);
        double oldxplane = _xplane;
        _xplane = _xplane * cos(_rotspeed) - _yplane * sin(_rotspeed);
        _yplane = oldxplane * sin(_rotspeed) + _yplane * cos(_rotspeed); 
    }

    void back(){
        if(_world.getwall(int(_xpos - _xdir * _movespeed),int(_ypos))==false){
            _xpos -= _xdir * _movespeed;
        }
        if(_world.getwall(int(_xpos),int(_ypos - _ydir * _movespeed)) == false){
            _ypos -= _ydir * _movespeed;
        }
    }

    void right(){
        //both camera direction and camera plane must be rotated
        double oldxdir = _xdir;
        _xdir = _xdir * cos(-_rotspeed) - _ydir * sin(-_rotspeed);
        _ydir = oldxdir * sin(-_rotspeed) + _ydir * cos(-_rotspeed);
        double oldxplane = _xplane;
        _xplane = _xplane * cos(-_rotspeed) - _yplane * sin(-_rotspeed);
        _yplane = oldxplane * sin(-_rotspeed) + _yplane * cos(-_rotspeed);
    }

};

class Window{
    private:
        std::string _title;
        int _width;
        int _height;
        int _quit = 0;
 
        SDL_Event _event;
        SDL_Window* _window = NULL;
        Camera* _camera = NULL;

    public:
        Window(std::string t,int w, int h){
            _title = t;
            _width = w;
            _height = h;

            initwindow();
            pollevents();
        }
        ~Window(){
            SDL_DestroyWindow(_window);
            SDL_Quit();
        }

        int initwindow(){
            if(SDL_Init(SDL_INIT_VIDEO)!=0){
                std::cerr << "Failed to init video"<<std::endl;
                return 0;
            }

            _window = SDL_CreateWindow
            (
            _title.c_str(), SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            SCREEN_WIDTH,
            SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN
            );
            if(_window ==NULL){
                std::cerr<<"Failed to create window"<<std::endl;
                return 0;
            }
            
            _camera = new Camera(_window);
        }

        void pollevents(){
            // event handler
            while(!_quit){
                const Uint8* keystate = SDL_GetKeyboardState(NULL);
                // continuous-response keys
                if(keystate[SDL_SCANCODE_W]){
                    _camera->forward();
                }
                if(keystate[SDL_SCANCODE_A]){
                    _camera->left();
                }
                if(keystate[SDL_SCANCODE_S]){
                    _camera->back();
                }
                if(keystate[SDL_SCANCODE_D]){
                    _camera->right();
                }

                    // handle events on queue
                    while( SDL_PollEvent( &_event) != 0 ){
                        if( _event.type == SDL_QUIT ){
                            _quit = 1;
                        }
                        else if( _event.type == SDL_KEYDOWN )
                        {
                            switch( _event.key.keysym.sym )
                            {
                                case SDLK_ESCAPE:
                                    _quit=1;
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                    _camera->raycast();
                    _camera->fps();

                // frame code here
            }
        }



};

int main (int argc, char** argv)
{
    Window("Raycaster",SCREEN_WIDTH,SCREEN_HEIGHT);

    return 0;
}